var doc = app.activeDocument;
var layer = doc.activeLayer;
var markers = layer.splineItems;

for (var i = 0; i < markers.length; i++) {
    markers[i].insertLabel('Number', '');
    markers[i].insertLabel('ExpectingColorName', '');
    markers[i].insertLabel('NumberSign', '');
    markers[i].insertLabel('NumberBoxId', '');
    markers[i].insertLabel('MessBoxId', '');
}