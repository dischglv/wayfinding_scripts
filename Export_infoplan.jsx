﻿var scriptsFolder = app.scriptPreferences.scriptsFolder;

$.evalFile(scriptsFolder + '/wayfinding_scripts/tech/common.jsx');
$.evalFile(scriptsFolder + '/wayfinding_scripts/tech/checkMess.jsx');
$.evalFile(scriptsFolder + '/wayfinding_scripts/tech/dataManipulations.jsx');
$.evalFile(scriptsFolder + '/wayfinding_scripts/tech/JSON-js-master/json2.js');

var pathToJsonXls = scriptsFolder + '/wayfinding_scripts/not_indesign/json2xls.py';

var xlsxPath = scriptsFolder + encodeURI('/wayfinding_scripts/node_modules/xlsx/dist/xlsx.extendscript.js');
var xlsxFile = File(xlsxPath);
if(xlsxFile.exists)
    $.evalFile(xlsxFile);

var data = [];
app.doScript(main, undefined, undefined,
    UndoModes.entireScript, "ExportCSV");
// main();

function main() {
    var docIn = app.activeDocument;
    var layers = docIn.layers;
    var dataAsObj = {};
    dataAsObj.types = [];
    dataAsObj.data = {};

    var w = new Window("palette", "Progress...");
    var progress = progressBar(w, layers.length);
    progress.maxvalue = layers.length;
    progress.value = 0;
    var files = 0;
    var wb = XLSX.utils.book_new();
    for (var l = 0; l < layers.length; ++l) {
        progress.value++;
        var L = layers[l];
        if (!L.visible)
            continue;

        data.length = 0;

        collectDataCsvNoStaff(L.splineItems);
        sortMatrix(data, 0, compNumberingLabels);

        if (data.shape - 2 <= 0) {
            alert('Ошибка\n'
                + 'Чтобы скрипт сработал, нужно оставить включенными '
                + 'только слои с объектами, для которых выгружается таблица.\n'
                + 'Также ошибка возникает, если включены слои без объектов.');
            return;
        }

        var header = generateHeader(L, data.shape - 2, ['id']);

        var csvPath = getCsvPath(docIn, L.name);
        var csvFile = new File(csvPath);
        writeCSV_header(csvFile, data, header, [1, 1]);
        ++files;

        writeXLSX_header(L.name, data, header, [1, 1], wb);
    }
    var xlsxPath = getXLSXPath(docIn);
    XLSX.writeFile(wb, xlsxPath);
    alert('Создано файлов csv: ' + files + '\nи 1 файл xlsx\n(в папке с исходным документом)');
}

